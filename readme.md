# LaTeX шаблоны

## Шаблоны в репозитории

| Название                                                                  | Источник                                                                                        |
| ---------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------- |
| [Шаблон для создания latex документов/рефератов по ГОСТ](essay_gost/)  | [ledovsky/latex-gost-template](https://github.com/ledovsky/latex-gost-template)                 |
| [Отчет по лабораторным работам](lab_report/)                           | [LaTeX Templates](http://www.latextemplates.com/template/university-school-laboratory-report)   |
| [Заметки к лекциям, конспекты](lecture_notes/)                         | [ShareLatex](https://ru.sharelatex.com/templates/university/lecture-notes-in-computer-sciences) |


## Полезные ссылки

+ [МЭИ - LaTeX, Maple. Программы, советы](http://vuz.exponenta.ru/PDF/dnld.html)
+ [LaTeX / Шаблоны ЕКСД и ГОСТ 7.32 для Lyx 1.6.x](http://bee.compkaluga.ru/forum/index.php?showtopic=33707)
+ [Больше TeX-примеров, красивых и разных (МАИ 8)](http://faq8.ru/read.php?2,11289,11430)
+ [Шаблон LaTeX v0.92](http://www.twirpx.com/file/156887/)
+ [LaTeX-шаблон для русской кандидатской диссертации и её автореферата](https://github.com/AndreyAkinshin/Russian-Phd-LaTeX-Dissertation-Template)
+ [LaTeX Templates](http://www.latextemplates.com/)